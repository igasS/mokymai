package database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.Scanner;

public class Database {
    public static void main(String[] args) {
        String url = "jdbc:mysql://0.0.0.0:8080/";
        String dbName = "practice";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "Terabaitas";
        Connection conn = null;
        Statement statement = null;
        ResultSet result = null;
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + dbName, userName, password);
        } catch (Exception e) {
            throw new RuntimeException("Can't connect to database", e);
        }
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter mysql insert statement:");
            String insert = scanner.nextLine();
            statement = (Statement) conn.createStatement();
            statement.executeUpdate(insert);
            System.out.println("Enter mysql select statement:");
            String select = scanner.nextLine();
            result = statement.executeQuery(select);
            while (result.next()) {
                System.out.println(result.getInt("id") + " " + result.getString("firstName") + " " + result.getString("lastName"));
            }
            conn.close();
        } catch (Exception e) {
            throw new RuntimeException("Something went wrong with queries to database", e);
        }

        finally {
            try {

                if (result != null) {
                    result.close();
                }

                if (statement != null) {
                    statement.close();
                }

                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException e) {
                throw new RuntimeException("Couldn't close connection or statement or result set", e);
            }
        }
    }
}
