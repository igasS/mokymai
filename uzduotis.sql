CREATE TABLE person(

    id INTEGER NOT NULL auto_increment, 

    firstName VARCHAR(50) NOT NULL,

    lastName VARCHAR(50) NOT NULL, 

    PRIMARY KEY(id)
);

CREATE TABLE contact(

    id INTEGER NOT NULL auto_increment,

    phone_number VARCHAR(50),

    address VARCHAR(100),

    PRIMARY KEY(id)
);



CREATE TABLE relation_category(

    id INTEGER NOT NULL auto_increment,

    type varchar(50) NOT NULL,

    PRIMARY KEY(id)
);

create table person_contact_relation(
    
    person_id integer not null,
    
    contact_id integer not null,

    foreign key(person_id) references person(id),

    foreign key(contact_id) references contact(id) 
);

create table person_relation_category_relation(

    person_id integer not null,

    relation_category_id integer not null,

    foreign key(person_id) references person(id),

    foreign key(relation_category_id) references relation_category(id)
);
